(function(){
//by wrapping the entire script in an IIFE, it becomes encapsulated and completely
//private from outside actors

function Question(question, answers, correctanswer){
    this.question = question;
    this.answers = answers;
    this.correctanswer = correctanswer;
}

Question.prototype.printQuestionAndAnswers = function() {
    console.log('-------------------------------------');
    console.log(this.question);
    let prefix = 0;
    for (let i = 0; i<this.answers.length;i++){
        console.log(prefix + ' - ' + this.answers[i]);
        prefix++;
    }
    console.log('-------------------------------------');
}

Question.prototype.isCorrect = function(attempt) {
    return attempt == this.correctanswer;
}

let questionOne = new Question('What is the best color?', ['Green', 'Red', 'Orange'], 0);
let questionTwo = new Question('What is Anna\'s last name?', ['Ramirez', 'Coleander', 'Caracheo', 'Chimken Nuggers'], 2);
let questionThree = new Question('What is the best meat?', ['Beef', 'Chicken', 'Pork', 'Tofu'], 1);
let questionFour = new Question('How many questions are there in this quiz?', ['one', 'two', 'three', 'four'], 3);

let questionArray = [];
questionArray.push(questionOne);
questionArray.push(questionTwo);
questionArray.push(questionThree);
questionArray.push(questionFour);



let playing = true;
let score = 0;

while (playing){
    //at the start of each round, reset guessing = true and get a new question number
    //and print the new question and answers
    let guessing = true;
    let questionNumber = Math.floor(Math.random()*questionArray.length);
    questionArray[questionNumber].printQuestionAndAnswers();

    //now let the player keep guessing until they have gotten it correctly
    while (guessing){
        let response = prompt('Enter the number of the correct answer');
        if (questionArray[questionNumber].isCorrect(response)){
            console.log('Correct!')
            score++;
            console.log('Your current Score is ' + score)
            guessing = false;
        } else if (response=='quit'){
            guessing = false;
            playing = false;
            console.log('Thanks for playing! Your final score was ' + score);
        }
    }
}



})();