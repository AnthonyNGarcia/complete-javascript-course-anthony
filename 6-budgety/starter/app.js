
// BUDGET CONTROLLER
let budgetController = (function() {
    // some code

    let Expense = function(id, description, value){
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = -1;
    };

    Expense.prototype.calculatePercentage = function(totalIncome){
        if (totalIncome > 0){
            this.percentage = Math.round((this.value / totalIncome) * 100);
        } else {
            this.percentage = -1;
        }
    };

    Expense.prototype.getPercentage = function() {
        return this.percentage;
    };

    let Income = function(id, description, value){
        this.id = id;
        this.description = description;
        this.value = value;
    };

    let calculateTotals = function(type) {
        let sum = 0;
        
        data.allItems[type].forEach(function(curr){
            sum += curr.value;
        });

        data.totals[type] = sum;
    
    };

    let data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1

    };

    return {
        addItem: function(type, des, val){

            let newItem, ID;
            //generate a unique ID for this item
            if (data.allItems[type].length > 0){
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                ID = 0;
            }

            if (type==='exp'){
                newItem = new Expense(ID, des, val);
            } else if (type === 'inc'){
                newItem = new Income(ID, des, val);
            }

            data.allItems[type].push(newItem);

            return newItem;
        },
        
        deleteItem: function(type, idDelete){
            //given the type and ID, delete the item from the data structure

            //The item is a value in the data.allItems.type[id]
            //first get the index of the type-id element. this is an Item object which has an id attribute
            //So we need to grab the index of the object whose ID matches the ID provided
            /* let arr = data.allItems[type]; //.splice(id, 1);
            console.log(arr[id]);
            arr.splice(arr.indexOf(arr.id), 1); */

            let ids = data.allItems[type].map(function(curr){
                return curr.id;
            });

            let ind = ids.indexOf(idDelete);

            if (ind !== -1){
                data.allItems[type].splice(ind, 1);
            }
            
        },

        calculateBudget: function() {
            
            //Calculate totals
            calculateTotals('inc');
            calculateTotals('exp');

            //Calculate budget
            data.budget = data.totals.inc - data.totals.exp;

            //Calculate percentage (if there is income > 0);
            if (data.totals.inc > 0){
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            } else {
                data.percentage = -1;
            }
            
        },

        calculatePercentages: function(){

            //we should iterate through each expense to calculate their percentage of the total income
            //each expense's value should be divided by the total expenses

            data.allItems.exp.forEach(function (cur) {
                cur.calculatePercentage(data.totals.inc);
            });

        },

        getPercentages: function() {

            let percentages = data.allItems.exp.map(function(cur) {
                return cur.getPercentage();
            });

            return percentages;
        },

        getBudget: function() {
            //return an object containing totals, budget, and percentage
            return {
                incTotal: data.totals.inc,
                expTotal: data.totals.exp,
                budget: data.budget,
                percentage: data.percentage
            }
        },

        testing: function() {
            console.log(data);
        }
    };
})();

// UI CONTROLLER
let UIController = (function(){
    // some code

    let DOMStrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeList: '.income__list',
        expensesList: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        deleteBtnContainer: '.container',
        expensesPercentageLabel: '.item__percentage',
        monthYearLabel: '.budget__title--month'
    };

    let formatNumber =  function(num, type) {
        //Will format a number into style:
        // + 1,200.56 OR - 1,200.56 based on type (+ or -)

        num = num.toFixed(2); //returns a string, which we can use string methods on, with 2 decimals, rounded

        splitNum = num.split('.');
        let int = splitNum[0];
        let dec = splitNum[1];

        //Now we have split the float into integer and decimal
        //Next we have to add the comma into the integer
        //The comma should only be placed into the int IF there are more than 3 digits, aka length > 3

        if (int.length > 3) {
            // The comma  should be placed 3 digits from the end of the number
            //We can easily reconstruct int by reassigning it to substrings of itself, but inserting a comma
            int = int.substr(0, int.length-3) + ',' + int.substr(int.length-3, 3);
        }


        /* //Now determine whether to start the number with a + or - based off type
        //Such a single-line variable definition can be more easily/cleanly done using the ternary operator
        let sign = (type=='inc') ? sign == '+' : sign == '-';
        //Finally we reconstruct and return our string
        return sign + ' ' + int + '.' + dec; */

        //The above can be done even more concisely by skipping sign and putting the ternary result straight into the return statement
        return (type=='inc' ? '+' : '-') + ' ' + int + '.' + dec;

    };

    let iterativeNodeForEachFunction = function(nodes, callbackFn){
        for (let i = 0; i<nodes.length; i++){
            callbackFn(nodes[i], i);
        }
    };

    return {
        getInput: function() {
            //
            return {
                type: document.querySelector(DOMStrings.inputType).value,
                description: document.querySelector(DOMStrings.inputDescription).value,
                value: parseFloat(document.querySelector(DOMStrings.inputValue).value)
            }
        },

        getDOM: function() {
            return DOMStrings;
        },

        clearFields: function(){
            let fields, arrFields;

            fields = document.querySelectorAll(DOMStrings.inputDescription + ', ' + DOMStrings.inputValue);

            arrFields = Array.prototype.slice.call(fields);

            arrFields.forEach(function(curr) {
                curr.value = "";
            });

            arrFields[0].focus();
        },

        displayBudget: function(obj){
            //This function will take in the passed budget object to update the UI with the passed values

            let type = obj.budget > 0 ? 'inc' : 'exp';

            document.querySelector(DOMStrings.budgetLabel).textContent = formatNumber(obj.budget, type);
            document.querySelector(DOMStrings.incomeLabel).textContent = formatNumber(obj.incTotal, 'inc');
            document.querySelector(DOMStrings.expensesLabel).textContent = formatNumber(obj.expTotal, 'exp');

            if (obj.percentage > 0){
                document.querySelector(DOMStrings.percentageLabel).textContent = obj.percentage + '%';
            } else {
                document.querySelector(DOMStrings.percentageLabel).textContent = '---';
            }
            
        },

        displayPercentages: function(percentages){

            let allExpenseNodes = document.querySelectorAll(DOMStrings.expensesPercentageLabel);

            iterativeNodeForEachFunction(allExpenseNodes, function(node, ind) {

                if (percentages[ind] > 0){
                    node.textContent = percentages[ind] + '%';
                } else {
                    node.textContent = '---';
                }

            });


        },

        changedType: function(){
            //This function will change the display/focus of the input fields/button
            //to either blue (income) and red(expenses) depending on the type of entry

            let fields = document.querySelectorAll(
                DOMStrings.inputType + ',' +
                DOMStrings.inputDescription + ',' +
                DOMStrings.inputValue
            );

            iterativeNodeForEachFunction(fields, function(cur){
                cur.classList.toggle('red-focus');
            })

            document.querySelector(DOMStrings.inputBtn).classList.toggle('red');
        },

        deleteListItem: function(itemID){
            //to do this, we want to remove html elements

            let element = document.getElementById(itemID);
            element.parentNode.removeChild(element);
        },

        displayMonthAndYear: function(){
            // This function will display for the user the current month and then year at the time of website start-up
            let now = new Date(); //left empty, this constructor defaults to containing the properties of the current time/time at run-time

            let month = now.getMonth(); //returns 0-11 based on month, where 0 is January and 11 is December

            //To conver this to a string, we'll use it as an index in the following reference array of all 12 months
            //These months are listed in chronological order from January to December to pair properly with month

            let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            //returns the current year in XXXX format
            let year = now.getFullYear();

            document.querySelector(DOMStrings.monthYearLabel).textContent = months[month] + ' ' + year;

        },

        addListItem: function(item, type){
            let html, element;

            //grab the HTML string template

            if (type==='inc'){
                html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%des%</div><div class="right clearfix"><div class="item__value">%val%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
                element = DOMStrings.incomeList;
            } else if (type==='exp'){
                html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%des%</div><div class="right clearfix"><div class="item__value">%val%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
                element = DOMStrings.expensesList;
            }

            //replace the dummy values in the template with our actual object values
            html = html.replace('%id%', item.id);
            html = html.replace('%des%', item.description);
            html = html.replace('%val%', formatNumber(item.value, type));

            //insert our newly created HTML into the DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', html);
            
        }

    }
})();

// GLOBAL UI CONTROLLER
let controller = (function(bdgtCtrl, UICtrl){
    //some code
    let setUpEventListeners = function(){
        let CtrlDOM = UICtrl.getDOM();
        document.querySelector(CtrlDOM.inputBtn).addEventListener('click', ctrlAddItem);
        document.addEventListener('keypress', function(event){
            if (event.keyCode === 13 || event.which === 13) //13 is the code for enter/return
                ctrlAddItem();
        });

        document.querySelector(CtrlDOM.deleteBtnContainer).addEventListener('click', ctrlDeleteItem);

        document.querySelector(CtrlDOM.inputType).addEventListener('change', UICtrl.changedType);
    };

    let updateBudget = function() {
        // 4. Calculate the Budget
        bdgtCtrl.calculateBudget();
        // 5. Return the Budge
        let budget = bdgtCtrl.getBudget();
        // 6. Display the updated budget on the UI
        //console.log(budget);
        UICtrl.displayBudget(budget);

    }

    let updatePercentages = function() {
        // 1. Calculate percentages
        bdgtCtrl.calculatePercentages();

        // 2. Get percentages from budget controller data structure
        let percentages = bdgtCtrl.getPercentages();

        // 3. Display updated percentages on the UI
        UICtrl.displayPercentages(percentages);
    };
    
    let ctrlAddItem = function(){

        // 1. Get the field input data and validate before proceeding

        let userInput = UICtrl.getInput();

        if (userInput.description!== "" && !(isNaN(userInput.value)) && userInput.value>0){
            // 1. Add the item to the budget controller
            let newItem = bdgtCtrl.addItem(userInput.type, userInput.description, userInput.value);

            // 2. Add the new item to the user interface
            UICtrl.addListItem(newItem, userInput.type);
            UICtrl.clearFields(); //clear the fields and reset focus as well

            // 3. Calculate and update the budget
            updateBudget();

            // 4. Update the percentages
            updatePercentages();
        }

    }

    let ctrlDeleteItem = function(event){
        let itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;

        if (itemID){
            let splitID = itemID.split('-');

            let type = splitID[0];

            let ID = parseInt(splitID[1]);

            //Delete the item from the data structure

            bdgtCtrl.deleteItem(type, ID);

            //Delete the item from the UI

            UICtrl.deleteListItem(itemID);

            //Update and show the new budget and percentages

            updateBudget();

            updatePercentages();
        }

        
    }

    return {
        init: function(){
            console.log('Initializing application . . .');
            setUpEventListeners();
            UICtrl.displayMonthAndYear();
            UICtrl.displayBudget({
                incTotal: 0,
                expTotal: 0,
                budget: 0,
                percentage: -1
            });
        }
    };

})(budgetController, UIController);

controller.init();