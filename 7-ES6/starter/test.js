function Person(name){
    this.name=name;
}

Person.prototype.myFriends6 = function(friends) {
    return friends.map( el => `${this.name} is friends with ${el}`);
} 

var friends = ['bob', 'kane', 'jim', 'lula'];

console.log(new Person('John').myFriends6(friends));