/////////////////////////////////
// CODING CHALLENGE

/*

Suppose that you're working in a small town administration, and you're in charge of two town elements:
1. Parks
2. Streets

It's a very small town, so right now there are only 3 parks and 4 streets. All parks and streets have a name and a build year.

At an end-of-year meeting, your boss wants a final report with the following:
1. Tree density of each park in the town (forumla: number of trees/park area)
2. Average age of each town's park (forumla: sum of all ages/number of parks)
3. The name of the park that has more than 1000 trees
4. Total and average length of the town's streets
5. Size classification of all streets: tiny/small/normal/big/huge. If the size is unknown, the default is normal

All the report data should be printed to the console.

HINT: Use some of the ES6 features: classes, subclasses, template strings, default parameters, maps, arrow functions, destructuring, etc.

*/

class Park{
    constructor(name, trees, area, buildyear) {
        this.name=name;
        this.trees=trees;
        this.area=area;
        this.buildyear=buildyear;
    }

    calculateTreeDensity(){
        return this.trees/this.area;
    }

    calculateAge(){
        let age = new Date().getFullYear() - this.buildyear
        return age;
    }

}

class Street{
    constructor(name, length, buildyear, classification= 'normal'){
        this.name=name;
        this.length=length;
        this.buildyear = buildyear;
        this.classification=classification;
    }
}

function printParkInfo(...parks) {
    console.log('----------PARKS REPORT---------------')
    let totalAge = 0;
    let count = 0;
    let thickParks = [];
    for (park of parks) {
        totalAge += park.calculateAge();
        count++;
        console.log(`${park['name']} has a tree density of: ${park.calculateTreeDensity()} trees/km`);
        if (park['trees'] > 1000) {
            thickParks.push(park);
        }
    }
    console.log(`The average age of our ${count} town parks is ${totalAge/count} years old.`);
    thickParks.forEach((park)=>{
        console.log(`${park['name']} has ${park['trees']} trees.`);
    })
}

function printStreetInfo(...streets) {
    console.log('----------STREETS REPORT---------------')
    let totalLength = 0;
    let count = 0;
    for (street of streets) {
        totalLength += street['length'];
        count++;
        console.log(`${street['name']}, built in ${street['buildyear']}, is a ${street['classification']} street.`);
    }
    console.log(`Our city's ${count} streets span a total of ${totalLength} km, with an average street length of ${totalLength/count} km.`);
}

let park1 = new Park('Sunset Park', 2000, 350, 1995);
let park2 = new Park('Ranchen Groves', 980, 240, 2005);
let park3 = new Park('Heartlands', 1200, 600, 2019);

let street1 = new Street('Central',14, 1975, 'huge');
let street2 = new Street('Offlane', 3, 1999, 'small');
let street3 = new Street('Ranchen Ave', 6, 2007)
let street4 = new Street('Newly Street', 2, 2020, 'tiny');

let parks = [park1, park2, park3];
let streets = [street1, street2, street3, street4];


printParkInfo(...parks);
printStreetInfo(...streets);