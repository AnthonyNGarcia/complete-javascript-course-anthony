//Coding Challenge 2:

let maryAverage = (92 + 131 + 105) / 3;
let mikeAverage = (117 + 95 + 124) / 3;
let johnAverage = (89 + 120 + 103) / 3;
if (mikeAverage >= johnAverage && mikeAverage >= maryAverage){
    if (mikeAverage == johnAverage && mikeAverage == maryAverage)
        console.log('We have a  three-way tie with a score of ' + mikeAverage + ' points!')
    else if (mikeAverage == johnAverage || mikeAverage == maryAverage){
        if (mikeAverage == johnAverage)
            console.log('Mike and John tie with a score of ' + mikeAverage + ' points!');
        else
            console.log('Mike and Mary tie with a score of ' + mikeAverage + ' points!');
    } else
        console.log('Mike\'s team wins with an average score of ' + mikeAverage + ' points!');
} else if (johnAverage >= mikeAverage && johnAverage >= maryAverage){
    if (johnAverage == maryAverage)
        console.log('Mary and John tie with a score of ' + maryAverage + ' points!');
    else
        console.log('John\'s team wins with an average score of ' + johnAverage + ' points!');
} else if (maryAverage >= mikeAverage && maryAverage >= johnAverage){
    console.log('Mary\'s team wins with an average score of ' + maryAverage + ' points!')
} else {
    console.log('No clear winner?')
}

console.log('John: ' + johnAverage)
console.log('Mike: ' + mikeAverage)
console.log('Mary: ' + maryAverage)