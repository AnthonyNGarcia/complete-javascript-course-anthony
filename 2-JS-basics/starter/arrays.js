let billArray = [124, 48, 268];

let tipArray = billArray.map(calculateTipFunction);

function calculateTipFunction(value){
    if (value<50)
        return value*20/100;
    else if (value>200)
        return value*10/100;
    else
        return value*15/100;
}

let billPlusTipArray = billArray.map(addTipFunction);

function addTipFunction(value, index){
    return value+tipArray[index];
}

function calculateAverage(...value){
    let varArray = value;
    let sum = 0;
    for (let i = 0; i<varArray.length;i++){
        sum += varArray[i];
    }
    return sum/varArray.length;
}

console.log(billArray);
console.log(tipArray);
console.log(billPlusTipArray);
console.log(calculateAverage(1,1,9,1,23,5,2));