let nameVar = 'window name';

let myObj = {
    nameVar:'myObj name',
    checkThis: function(){
        console.log('the name variable associated with ' + this + ' is ' + this.nameVar);

        function innerFunction(){
            console.log('the name variable associated with ' + this + ' is ' + this.nameVar);
        }

        innerFunction();
    }
}

myObj.checkThis();