var john = {
    name:'John',
    weight:120,
    height:8,
    getBmi: function() { 
        this.bmi = this.weight/this.height^2;
        return this.bmi;
    }
}

var mark = {
    name:'Mark',
    weight:200,
    height:8,
    getBmi: function() { 
        this.bmi = this.weight/this.height^2;
        return this.bmi;
    }
}

john.getBmi();
mark.getBmi();

var johnHasMoreBMI = john.bmi > mark.bmi;

console.log('Does John have a higher BMI than Mark? ' + johnHasMoreBMI);

var higherBMI = john.bmi > mark.bmi ? 'John' : 'Mark';

console.log(higherBMI + ' has a higher BMI.');